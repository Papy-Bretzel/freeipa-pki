# Ansible Role Freeipa Config

This role will create freeIPA users, services and roles, generating and deploying certificates when needed

# TODO

* la génération de certificats pour les services est problématique car les services Kerberos sont obligatoirement associés à un host.  
  * simplifier la création des certificats (uniquement une var "create" à true ou false)
  * créer une variable `ipa_certificates` pour lister les déclarations de certificats.
  * devra contenir en plus des variables définies plus bas :
    * common name (obligatoire)
    * hosts (sur lesquels ils seront déployés.)
  * voir utilisation de NSSDB pour le déploiement sur les bons hôtes
  
 
## Vars

### Global vars
| Var name                   | Description                                                                                                                                             | Type   | Required ? | Default         |
| -------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------- | ------ | ---------- | --------------- |
| ipa_server                 | FQDN of FreeIPA Master server                                                                                                                           | string | no         | `localhost`     |
| ipa_user                   | username used to connect to FreeIPA master                                                                                                              | string | no         | `admin`         |
| ipa_password               | Password to connect to FreeIPA master                                                                                                                   | string | **yes**    |                 |
| ipa_realm                  | Main Kerberos realm                                                                                                                                     | string | no         |                 |
| ipa_validate_certs         | Should certificates be validated for connections to freeIPA. Set to `no` if you FreeIPA instance uses self-signed certificates.                         | bool   | no         | True            |
| ipa_cert_master_host       | Hostname / FQDN / Ansible name of the host responsible for certificate generation. Default is the first member of the `ipaservers` group.               | string | **yes**    | `ipaservers[0]` |
| ipa_cert_default_path      | Set the default path of generated certificates. Certificates will be stored with keys and CA in a sub directory of this path named after its principal. | string | no         | `/etc/ipa/tls`  |
| ipa_cert_renew             | This should be set to True for a certificate renewal                                                                                                    | bool   | no         | False           |
| ipa_cert_override_keys     | Set this value to true to re-generate certificate keys                                                                                                  | bool   | no         | False           |
| ipa_cert_key_length        | Length of the generated key for a service certificate                                                                                                   | number | no         | 4096            |
| ipa_cert_country_name      | Default Country Name for generated certificates                                                                                                         | string | no         |                 |
| ipa_cert_organization_name | Default Organization Name for generated certificates                                                                                                    | string | no         |                 |
| ipa_cert_email_address     | Default email address for generated certificates                                                                                                        | string | no         |                 |

### User vars
| Var name    | Description                                                                | Type       | Required ? | Default   |
| ----------- | -------------------------------------------------------------------------- | ---------- | ---------- | --------- |
| ipa_users   | List of users of FreeIPA.                                                  | list[dict] | no         |           |
| user.name   | User unique name                                                           | string     | **yes**    |           |
| user.passwd | User password. If provided, will override the potential existing password. | string     | no         |           |
| user.state  | State to ensure. `present` or `abstent`                                    | enum       | no         | `present` |


### Services vars 

| Var name          | Description                                                                                                                               | Type         | Required ? | Default   |
| ----------------- | ----------------------------------------------------------------------------------------------------------------------------------------- | ------------ | ---------- | --------- |
| ipa_services      | List of services of FreeIPA                                                                                                               | list[dict]   | no         |           |
| service.principal | Kerberos principal of the service                                                                                                         | string       | **yes**    |           |
| service.state     | State to ensure. `present` or `abstent`                                                                                                   | enum         | no         | `present` |
| service.hosts     | defines the list of `ManagedBy` hosts                                                                                                     | list[string] | no         |           |
| service.cert      | Information about certificate attached to the service. Check certificate dict format in ['Certificates vars'](#certificates-vars) section | dict         | no         |           |

### Certificates vars

<!-- > **Note :** Certificates can be declared for users and services, in their respective `cert` field. -->

| Var name               | Description                                                                                                                                                                                                                                                          | Type         | Required ? | Default |
| ---------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------ | ---------- | ------- |
| cert.cn                | Common name for the certificate. Default to parrent object 'principal' field.                                                                                                                                                                                        | string       | no         |         |
| cert.alt_names         | List of alternative DNS names for the certificate                                                                                                                                                                                                                    | list[string] | no         |         |
| cert.renew             | Individual trigger to know if cert should be renewed. Defaults to global statement `ipa_cert_renew`                                                                                                                                                                  | bool         | no         | False   |
| cert.override_key      | Individual trigger to re-generate a specific private key. Defaults to global statement `ipa_cert_override_key`                                                                                                                                                       | bool         | no         | False   |
| cert.key_length        | Generated key length. Defaults to global statement `ipa_cert_key_length`                                                                                                                                                                                             | number       | no         |         |
| cert.country_name      | Certificate country name. Defaults to global statement `ipa_cert_country_name`                                                                                                                                                                                       | string       | no         |         |
| cert.organization_name | Certificate organization name. Default to global statement `ipa_cert_organization_name`                                                                                                                                                                              | string       | no         |         |
| cert.email_address     | Certificate email address. Defaults to global statement `ipa_cert_email_address`                                                                                                                                                                                     | string       | no         |         |
| cert.path              | Path of the directory containing all certs files (keys, certs and chain files). Defaults to global statement `ipa_cert_default_path`. Cert files will be deployed in a subdirectory of this path named after the principal name, by replacing optionnal `/` by `---` | string       | no         |         |
| cert.raw               | Dictionnary containing certificate raw data at PEM format. If provided, prevents certificate generation.                                                                                                                                                             | dict         | no         |         |
| cert.raw.crt           | Certificate data at PEM format.                                                                                                                                                                                                                                      | string       | no         |         |
| cert.raw.key           | Certificate key at PEM format.                                                                                                                                                                                                                                       | string       | no         |         |
| cert.raw.chain         | Certificate CA - chain at PEM format.                                                                                                                                                                                                                                | string       | no         |         |

### Roles vars

| Var name         | Description                                  | Type         | Required ? | Default   |
| ---------------- | -------------------------------------------- | ------------ | ---------- | --------- |
| ipa_roles        | List of roles of FreeIPA                     | list[dict]   | no         |           |
| role.name        | Name of the role                             | string       | **yes**    |           |
| role.description | Description of the role                      | string       | no         |           |
| role.hosts       | List of hosts to assign to this role         | list[string] | no         |           |
| role.privileges  | List of privilege granted to the role.       | list[string] | no         |           |
| role.services    | List of service names to assign to this role | list[string] | no         |           |
| role.state       | State to ensure. `present` or `abstent`      | enum         | no         | `present` |